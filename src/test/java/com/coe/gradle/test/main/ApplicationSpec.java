package com.coe.gradle.test.main;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ApplicationSpec {
    @Test
    void testGetMessage(){
        String name="hussain";
        Application app=setupTestObject();
        String message=app.getMessage(name);
        assertEquals("welcome "+name,message);
    }

    Application setupTestObject(){
        class ApplicationTest extends Application{
        }
        return new ApplicationTest();
    }



}
