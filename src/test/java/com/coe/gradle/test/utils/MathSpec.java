package com.coe.gradle.test.utils;

import com.coe.gradle.test.main.Application;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MathSpec {

    @Test
    void add() {
        Math util = setupTestObject(false);
        assertEquals(5.0,util.add(1,4));
    }

    @Test
    void sub() {
        Math util = setupTestObject(false);
        assertEquals(3.0,util.sub(4,1));
    }

    @Test
    void mutiply() {
        Math util = setupTestObject(false);
        assertEquals(8.0,util.mutiply(2,4));
    }

    @Test
    void divInt(){
        Math util = setupTestObject(true);
        assertEquals(3.0,util.div(7,2));
    }

    @Test
    void divFloat(){
        Math util = setupTestObject(false);
        assertEquals(3.5,util.div(7,2));
    }

    Math setupTestObject(boolean shouldShowIntDev){
        class MathTest extends Math{
        }
        MathTest testObj = new MathTest();
        testObj.setShowIntDev(shouldShowIntDev);
        return testObj;
    }
}