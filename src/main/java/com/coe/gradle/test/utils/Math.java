package com.coe.gradle.test.utils;

public class Math {



    public boolean showIntDev;

    public void setShowIntDev(boolean showIntDev) {
        this.showIntDev = showIntDev;
    }

    public boolean isShowIntDev() {
        return showIntDev;
    }


    float add(float a , float b){
        return a+b;
    }

    float sub(float a , float b){
        return a-b;
    }

    float mutiply(float a , float b){
        return a*b;
    }

    float div(float a , float b){
        if(showIntDev){
            return (int) (a/b);
        }
        else{
            return a/b;
        }

    }

}
